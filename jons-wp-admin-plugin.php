<?php
/**
 * @wordpress-plugin
 * Plugin Name: Jon's WP Admin Plugin
 * Plugin URI: http://www.pixelmotion.com
 * Description: Something to help me handle the admin bar when logged in
 * Version: 1.0.0
 * Author: Jon Rasmussen
 */

/** If this file is called directly, abort. */
if (!defined('ABSPATH')) {
    die ('Plugin file cannot be accessed directly.');
}

function run(){
    wp_enqueue_script('jons-wp-admin-script', plugin_dir_url(__FILE__) . 'js/script.js');
}

if(!is_admin()) {
    run();
}