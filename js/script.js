setTimeout(function () {
    (function ($) {
        $(function () {
            var siteToken = pm_api.site_token;
            var $adminBar = $('#wpadminbar');
            var $button = $('<button>')
                .addClass('btn')
                .text('Show Admin Bar - ' + siteToken)
                .css({
                    position: 'fixed',
                    bottom: 0,
                    left: 0
                })
                .attr('data-hidden', 1)
                .click(function () {
                    var text = $button.text();
                    if (text.substring(0, 3) === 'Log') {
                        location.replace('/wp-admin');
                    } else {
                        if ($button.attr('data-hidden') == 1) {
                            $adminBar.show();
                            $button.attr('data-hidden', 0).text('Hide Admin Bar - ' + siteToken);
                        } else {
                            $adminBar.hide();
                            $button.attr('data-hidden', 1).text('Show Admin Bar - ' + siteToken);
                        }
                    }
                });

            if ($adminBar[0]) {
                $adminBar.hide();
            } else {
                $button.text('Log In - ' + siteToken);
            }

            var $extraButton = $('<button>')
                .addClass('btn btn-info')
                .text('Extra Button')
                .css({
                    position: 'fixed',
                    bottom: 0,
                    right: 0
                })
                .click(function () {
                    /** Add your custom functionality here **/
                });

            $('body').append($button, $extraButton);
        });
    })(jQuery);
}, 2000);